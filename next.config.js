const path = require('path');
const {VanillaExtractPlugin} = require('@vanilla-extract/webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  webpack: (config, {buildId, dev, isServer, defaultLoaders, webpack}) => {
    config.module.rules.push({
      test: /\.scss$/,
      use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
    });
    config.plugins.push(
        new VanillaExtractPlugin(),
        new MiniCssExtractPlugin({
          filename: 'static/chunks/[contenthash].css',
          chunkFilename: 'static/chunks/[contenthash].css',
          ignoreOrder: true,
        }),
    );
    return config;
  },
  reactStrictMode: true,
  srcDir: 'src',
  sassOptions: {
    includePaths: [path.join(__dirname, '/assets/styles')],
  },
};
