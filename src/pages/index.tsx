import {useEffect} from 'react';
import {getCurrentUser} from '@lib/useUser';
import {useRouter} from 'next/router';

const Home = (props: any) => {
  const router = useRouter();
  useEffect(() => {
    if (props.error) {
      router.push('/login');
    }
    if (props.user) {
      router.push('/@me');
    }
  }, [props, router]);
  return null;
};

export const getServerSideProps = getCurrentUser();

export default Home;
