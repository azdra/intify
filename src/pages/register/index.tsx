import React, {useEffect} from 'react';

const Login = (): JSX.Element => {
  const submitForm: React.FormEventHandler = (e) => {
    console.log('submit');
    e.preventDefault();
  };

  return (
    <form onSubmit={submitForm}>
      <div>
        <label htmlFor="email">EMAIL_ADDRESS</label>
        <input type="text" name="email" id="email"/>
      </div>
      <div>
        <label htmlFor="password">PASSWORD</label>
        <input type="password" name="password" id="password"/>
      </div>
      <div>
        <label htmlFor="password_confirmation">PASSWORD_CONFIRMATION</label>
        <input type="password" name="password_confirmation" id="password_confirmation"/>
      </div>
      <div>
        <button type="submit">LOGIN</button>
      </div>
    </form>
  );
};

export default Login;
