import React, {useEffect} from 'react';
import {getCurrentUser} from '@lib/useUser';
import {useRouter} from 'next/router';

const Me = (props: any) => {
  const router = useRouter();

  useEffect(() => {
    if (props.error) router.push('/login');
  }, [props, router]);
  return (
    <div>
      @me
    </div>
  );
};

export const getServerSideProps = getCurrentUser(true);

export default Me;
