import {useRouter} from 'next/router';
import React, {useEffect} from 'react';
import {getCurrentUser} from '@lib/useUser';
import {socketRequest} from '@lib/socketRequest';
import {GuildInterface} from '../../../interfaces/GuildInterface';

const Channel = (props: any) => {
  const router = useRouter();
  const {guild, channel} = router.query;
  const socket = socketRequest();

  useEffect(() => {
    if (props.error) router.push('/login');

    socket.emit('GET:DATA:GUILD', {guildID: guild, token: props.user.token}, (callback: GuildInterface) => {
      props.setInfoGuild(callback);
    });
  }, [router, channel]);

  return (
    <div>
      {guild}
    </div>
  );
};

export const getServerSideProps = getCurrentUser(true);

export default Channel;
