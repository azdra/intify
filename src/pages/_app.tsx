import type {AppProps} from 'next/app';
import React, {useEffect, useState} from 'react';
import '@styles/app.scss';
import style from './app.module.scss';
import Head from 'next/head';
import {ServerSide} from '@components/ServerSide/ServerSide';
import {socketRequest} from '@lib/socketRequest';
import {UserInterface} from '../interfaces/UserInterface';
import {ChannelInterface} from '../interfaces/ChannelInterface';
import {Channel} from '@components/Channel/Channel';
import {GuildInterface} from '../interfaces/GuildInterface';
import {useRouter} from 'next/router';

const MyApp = ({Component, pageProps}: AppProps): any => {
  const socket = socketRequest();
  const [user, setUser] = useState<UserInterface>();
  const [guild, setGuilds] = useState<GuildInterface|null>(null);
  const router = useRouter();

  useEffect(() => {
    if (pageProps.user) {
      socket.emit('@ME:GET:DATA', {token: pageProps.user.token}, (callback: any) => {
        setUser(callback);
      });
    }
  }, [pageProps.user, socket]);

  useEffect(() => {
    setGuilds(null);
  }, [router]);

  const setInfoGuild = (guild: GuildInterface) => {
    console.log(guild);
    setGuilds(guild);
  };

  return (
    <div className={style.container}>
      <Head>
        <title>Intify</title>
      </Head>

      {
        pageProps && pageProps.isApp ? <>
          <ServerSide {...pageProps}/>
        </> : null
      }

      <div className={style.content}>
        {
          pageProps && pageProps.isApp ? <>
            <div className={style.channelSideContainer}>
              <div id={'channel-container'} className={style.channelContainer}>
                {
                  guild?.name
                }
                {
                  guild?.channels.map((channel, i) => {
                    return <Channel {...channel} key={i}/>;
                  })
                }
              </div>
              <div className={style.userContainer}>
                <div className={'m-1'}>
                  {user?.username}
                </div>
              </div>
            </div>
          </> : null
        }

        <Component setInfoGuild={setInfoGuild} {...pageProps} />
      </div>
    </div>
  );
};

export default MyApp;
