import {NextApiRequest, NextApiResponse} from 'next';
import {withIronSession, Session} from 'next-iron-session';
import {sessionOptions} from '@lib/useUser';
type NextIronRequest = NextApiRequest & { session: Session };

/**
 * handler
 * @param {NextIronRequest} req
 * @param {NextApiResponse} res
 * @return {NextApiResponse}
 */
async function handler(
    req: NextIronRequest,
    res: NextApiResponse,
): Promise<void> {
  if (req.method !== 'POST') {
    return res.status(405).json('405 Method Not Allowed');
  }

  const data = JSON.parse(req.body);

  req.session.set('user', {
    id: data.id,
    token: data.token,
  });

  await req.session.save();
  res.json({'confirm': 'Logged in'});
}

export default withIronSession(handler, sessionOptions);
