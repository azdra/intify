import {Session, withIronSession} from 'next-iron-session';
import {NextApiRequest, NextApiResponse} from 'next';
import {sessionOptions} from '@lib/useUser';
type NextIronRequest = NextApiRequest & { session: Session };

/**
 * handler
 * @param {NextIronRequest} req
 * @param {NextApiResponse} res
 * @return {NextApiResponse}
 */
const handler = (
    req: NextIronRequest,
    res: NextApiResponse,
): void => {
  if (req.method !== 'POST') {
    return res.status(405).json('405 Method Not Allowed');
  }

  req.session.destroy();
  res.send('Logged out');
};

export default withIronSession(handler, sessionOptions);
