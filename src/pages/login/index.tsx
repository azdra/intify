import React, {useState} from 'react';
import {loginUser} from '@lib/useUser';
import {makeRequest} from '@lib/makeRequest';
import {useRouter} from 'next/router';

const Login = (): JSX.Element => {
  const router = useRouter();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const submitForm: React.FormEventHandler = async (e) => {
    e.preventDefault();

    makeRequest({
      method: 'POST',
      url: '/auth/login',
      data: {
        email,
        password,
      },
    })
        .then(async (r) => {
          if (r.data.code === 'loginAuthSuccess') {
            await loginUser(r.data.user);
            await router.push('/@me');
          }
        })
        .catch((e) => {
          console.log(e);
        });
    console.log('submit');
  };

  return (
    <form method={'POST'} onSubmit={submitForm}>
      <div>
        <label htmlFor="email">EMAIL_ADDRESS</label>
        <input value={email} onChange={(event) => setEmail(event.currentTarget.value) } type="text" name="email" id="email"/>
      </div>
      <div>
        <label htmlFor="password">PASSWORD</label>
        <input value={password} onChange={(event) => setPassword(event.currentTarget.value) } type="password" name="password" id="password"/>
      </div>
      <div>
        <button type="submit">LOGIN</button>
      </div>
    </form>
  );
};

export default Login;
