import {withIronSession} from 'next-iron-session';

export const sessionOptions = {
  password: String(process.env.IRON_PASSWORD),
  cookieName: String(process.env.IRON_COOKIE_NAME),
  cookieOptions: {
    secure: process.env.NODE_ENV === 'production',
  },
};

export const loginUser = async (data: any) => {
  const res = await fetch(`http://localhost:3000/api/login`, {
    method: 'POST',
    body: JSON.stringify(data),
  });
  return await res.json();
};

export const getCurrentUser = (isApp: boolean = false) => withIronSession(
    async ({req, res}: any) => {
      const user = req.session.get('user');

      if (!user) {
        return {
          props: {
            error: 'noConnected',
          },
        };
      }

      return {
        props: {
          user,
          isApp,
        },
      };
    }, sessionOptions,
);
