import axios, {AxiosPromise} from 'axios';

export interface MakeRequestInterface {
  method: 'GET'|'POST';
  url: string;
  data?: any;
  headers?: any;
}

/**
 * @param {MakeRequestInterface} config
 * @return {AxiosPromise}
 */
export const makeRequest = (config: MakeRequestInterface): AxiosPromise => {
  return axios({
    method: config.method,
    url: config.url,
    baseURL: 'http://localhost:3001/api/v1',
    data: config.data,
  });
};
