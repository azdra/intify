import React, {useEffect, useRef, useState} from 'react';
import {socketRequest} from '@lib/socketRequest';
import {AppPropsInterface} from '../../interfaces/AppPropsInterface';
import {GuildInterface} from '../../interfaces/GuildInterface';
import {ServerButton} from '@components/ServerButton/ServerButton';
import serverside from './serverside.module.scss';
import {HomeButton} from '@components/HomeButton/HomeButton';
import {PlusServerButton} from '@components/PlusServerButton/PlusServerButton';
import serverButton from '@components/ServerButton/serverbutton.module.scss';
import {useRouter} from 'next/router';

export const ServerSide = (props: AppPropsInterface) => {
  const socket = socketRequest();
  const [guilds, setGuilds] = useState<GuildInterface[]>([]);
  const homeRef = useRef<HTMLButtonElement>(null);
  const [selectedButton, setSelectedButton] = useState<HTMLButtonElement>();
  const router = useRouter();
  const {guild, channel} = router.query;

  useEffect(() => {
    socket.emit('USER:GET:GUILD', {
      token: props.user.token,
    }, (servers: GuildInterface[]) => {
      console.log(servers);
      setGuilds(servers);
    });

    if (homeRef.current && homeRef.current.parentElement) {
      setSelectedButton(homeRef.current);
      homeRef.current.parentElement.classList.add('server-selected');
    }
  }, [props.user.token, socket]);

  useEffect(() => {
    if (channel) {
      console.log(guilds);
    }
  }, [channel, guilds]);

  const style = `${serverButton.container} server-btn-server my-2 button mx-auto rounded-circle is-hover-rounded is-hover-bg-primary text-primary is-hover-text-light`;
  return (
    <div className={`${serverside.container}`}>
      <div className={`${serverside.list}`}>
        <div className={'my-2'}>
          <div className={`server-pill home-button-container position-relative`}>
            <HomeButton ref={homeRef} styles={style}/>
          </div>

          <div className={'divider-horizontal my-3 mx-auto w-50 rounded opacity-2'} />

          {
            guilds.length ? <div className={'server-list-container'}>
              {
                guilds.map((guild, i) => {
                  return <div className={`server-pill position-relative`} key={i}>
                    <ServerButton {...guild} styles={style}/>
                  </div>;
                })
              }
            </div> : null
          }

          <div className={'divider-horizontal my-3 mx-auto w-50 rounded opacity-2'} />

          <div className={`server-pill position-relative plus-button-container`}>
            <PlusServerButton styles={style}/>
          </div>
        </div>
      </div>
    </div>
  );
};
