import React from 'react';
import {ChannelInterface} from '../../interfaces/ChannelInterface';

export const Channel: React.FC<ChannelInterface> = (props: ChannelInterface): JSX.Element => {
  return (
    <div>
      {props.name}
    </div>
  );
};
