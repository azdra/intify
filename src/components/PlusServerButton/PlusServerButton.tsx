import React from 'react';
import {PlusIcon} from '@components/Icons/PlusIcon';

export const PlusServerButton = (props: {styles: string}): JSX.Element => {
  return (
    <button className={props.styles}>
      <div className={'d-flex w-100 h-100 justify-content-center align-items-center w-100 h-100'}>
        <PlusIcon/>
      </div>
    </button>
  );
};
