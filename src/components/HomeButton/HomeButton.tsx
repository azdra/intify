import React, {forwardRef} from 'react';
import {HomeIcon} from '@components/Icons/HomeIcon';
import {useRouter} from 'next/router';

interface HomeButtonProps {
  styles: string
}

export const HomeButton = forwardRef((props: HomeButtonProps, ref: React.LegacyRef<HTMLButtonElement>): JSX.Element => {
  const router = useRouter();
  const homeButton = () => {
    router.push('/@me');
  };

  return (
    <button onClick={homeButton} ref={ref} className={`${props.styles} home-button`}>
      <div className={'d-flex w-100 h-100 justify-content-center align-items-center w-100 h-100'}>
        <HomeIcon/>
      </div>
    </button>
  );
});
HomeButton.displayName = 'HomeButton';
