import React from 'react';
import {GuildInterface} from '../../interfaces/GuildInterface';
import {useRouter} from 'next/router';


interface ServerButtonInterface extends GuildInterface {
  styles: string
}

export const ServerButton = (props: ServerButtonInterface): JSX.Element => {
  const router = useRouter();
  const {guild, channel} = router.query;

  const clickServer = () => {
    if (props.id === guild) return;

    const firstTextChannel = props.channels.filter((channel) => channel.type === 'text')[0];
    router.push(`/channel/${props.id}/${firstTextChannel.id}`);
  };

  return (
    <button onClick={clickServer} className={props.styles}>

    </button>
  );
};
