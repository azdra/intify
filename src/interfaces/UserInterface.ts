import {ImageInterface} from './ImageInterface';

export interface UserInterface {
  createdAt: Date;
  updateAt?: Date;
  id: string;
  firstname: string;
  lastname: string;
  username: string;
  birthday?: Date;
  email: string;
  password: string;
  salt: string;
  roles: string;
  enabled: boolean;
  lastLogin?: Date;
  image?: ImageInterface
}
