export enum ChannelType {
  TYPE_TEXT = 'text',
  TYPE_DM = 'dm',
  TYPE_VOICE = 'voice',
  TYPE_CATEGORY = 'category',
}

export interface ChannelInterface {
  createdAt: Date;
  updateAt?: Date;
  id: string;
  name: string;
  topic?: string;
  parent?: number;
  order?: number;
  type: ChannelType;
  children: ChannelInterface[];
}
