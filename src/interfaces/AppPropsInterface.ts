export interface AppPropsInterface {
  isApp: boolean;
  user: {
    id: string,
    token: string,
  };
}
