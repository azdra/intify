import {GuildInterface} from './GuildInterface';

export interface ServersSectionInterface {
  onClick(guild: GuildInterface): void
}
