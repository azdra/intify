import {GuildInterface} from './GuildInterface';
import {ChannelInterface} from './ChannelInterface';
import {MemberInterface} from './MemberInterface';
import {ImageInterface} from './ImageInterface';

export interface MessageInterface {
  createdAt: Date;
  updateAt?: Date;
  id: string;
  content: string;
  guild?: GuildInterface;
  channel?: ChannelInterface;
  member: MemberInterface;
  messageAttachment?: ImageInterface[];
}
