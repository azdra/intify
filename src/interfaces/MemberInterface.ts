import {UserInterface} from './UserInterface';

export interface MemberInterface {
  createdAt: Date;
  id: number;
  joinAt: Date;
  name: string;
  updateAt?: Date;
  user: UserInterface;
}
