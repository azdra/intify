import {GuildInterface} from './GuildInterface';

export interface ServerButtonInterface {
  guild: GuildInterface;
  key: number
}
