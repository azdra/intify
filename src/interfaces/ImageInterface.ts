export interface ImageInterface {
  id: number;
  path: string;
  size: number;
  format: string;
  name: string;
}
