import {ChannelInterface} from './ChannelInterface';

export interface ChatSideInterface {
  channelInfo: ChannelInterface;
}
