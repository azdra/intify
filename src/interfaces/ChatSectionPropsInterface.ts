import {MessageInterface} from './MessageInterface';
import {RefObject} from 'react';

export interface ChatSectionPropsInterface {
  messages: MessageInterface[];
  scroll: RefObject<HTMLDivElement>;
  deleteMessage: Function;
}
