import {GuildInterface} from './GuildInterface';
import {ChannelInterface} from './ChannelInterface';

export interface BaseGuildInterface {
  guild: GuildInterface;
  channels: ChannelInterface[];
  selectChannel?: Function;
}
