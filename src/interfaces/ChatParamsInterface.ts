export interface ChatParamsInterface {
  guildId: string;
  channelId: string;
}
