import {ChannelInterface} from './ChannelInterface';
import {ImageInterface} from './ImageInterface';
import {MemberInterface} from './MemberInterface';

export interface GuildInterface {
  id: string;
  name: string;
  channels: ChannelInterface[];
  image?: ImageInterface;
  members: MemberInterface[];
  createdAt: Date;
  updateAt?: Date;
}
